#include "addresses.h"

#if defined(__GNUC__)
	#ifdef __clang__
		#define DISABLE_OPT __attribute__((noinline,optnone))
	#else
		#define DISABLE_OPT __attribute__((noinline,optimize("O0")))
	#endif // __clang__
#else
#define DISABLE_OPT
#endif // defined(__GNUC__)

int DISABLE_OPT RCT2_CALLPROC_X(int address, int _eax, int _ebx, int _ecx, int _edx, int _esi, int _edi, int _ebp)
{
	return 0;
}

int DISABLE_OPT RCT2_CALLFUNC_X(int address, int *_eax, int *_ebx, int *_ecx, int *_edx, int *_esi, int *_edi, int *_ebp)
{
	return 0;
}
