#include "../openrct2.h"

#ifdef __ANDROID__

#include <android/configuration.h>
#include <wchar.h>

int mbtowc(wchar_t *__restrict pwc, const char *__restrict s, size_t n) {
    static mbstate_t mbs;
    size_t rval;
    if (s == NULL) {
        /* No support for state dependent encodings. */
        memset(&mbs, 0, sizeof(mbs));
        return (0);
    }
    rval = mbrtowc(pwc, s, n, &mbs);
    if (rval == (size_t) -1 || rval == (size_t) -2)
        return (-1);
    return ((int) rval);
}

bool platform_check_steam_overlay_attached() {
    STUB();
    return false;
}

bool platform_open_common_file_dialog(utf8 *outFilename, file_dialog_desc *desc) {
    STUB();
    return false;
}

void platform_get_exe_path(utf8 *outPath)
{
    strcpy(outPath, "/sdcard/openrct2");
}

void platform_posix_sub_user_data_path(char *buffer, const char *homedir, const char *separator) {
    strcpy(buffer, "/sdcard/openrct2-user/");
}

void platform_show_messagebox(char *message)
{
    STUB();
}

utf8 *platform_open_directory_browser(utf8 *title)
{
    log_info(title);
    STUB();
    return "/sdcard/rct2";
}

bool platform_get_font_path(TTFFontDescriptor *font, utf8 *buffer)
{
    STUB();
    return false;
}

void platform_posix_sub_resolve_openrct_data_path(utf8 *out) {
    strcpy(out, "/sdcard/openrct2");
}

#endif
